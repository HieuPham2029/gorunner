package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestPingEndpoint(t *testing.T) {
	// Set up the Gin router
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "pong 4"})
	})

	// Create a request to the /ping endpoint
	req, err := http.NewRequest("GET", "/ping", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Create a response recorder to record the response
	res := httptest.NewRecorder()

	// Serve the request to the router
	r.ServeHTTP(res, req)

	// Check the status code
	assert.Equal(t, http.StatusOK, res.Code)

	// Check the response body
	expected := `{"message":"pong 4"}`
	assert.Equal(t, expected, res.Body.String())
}
